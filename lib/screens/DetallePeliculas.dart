
import 'package:dayanara_burgasi/model/pelicula.dart';
import 'package:flutter/material.dart';

class DetallePeliculas extends StatefulWidget {
  
    final Pelicula pelicula;
  
    final BuildContext context;
  
    const DetallePeliculas({Key? key, required this.pelicula, required this.context}) : super(key: key);
  
    @override
  
    _DetallePeliculaState createState() => _DetallePeliculaState();
  
  }

class _DetallePeliculaState extends State<DetallePeliculas> {
    @override
    Widget build(BuildContext context) {
      
    return Card(
      child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
      child:Column(
        children:<Widget> [
          Container(
            height: 140.0,
            width: 500.0,
            child: Image.asset(widget.pelicula.foto,height: 140.0,width:160.0),

          ),
          padding(Text(widget.pelicula.name ,textAlign: TextAlign.center,style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold,))),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child: padding(Text(widget.pelicula.duracion,textAlign: TextAlign.center ,style: TextStyle(fontSize: 14.0,color: Colors.black45))),

              )
              
            )
            
          ],),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child: padding(Text('Descripcion :  ' ,textAlign: TextAlign.center ,style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold))),  
              
              )
            )
            
          ],),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child: padding(Text( widget.pelicula.description  ,style: TextStyle(fontSize: 16.0))),  
              )
            )
            
          ],),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child: padding(Text('Personajes: ' ,textAlign: TextAlign.center , style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold))),  

              )
            )
            
          ],),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child: padding(Text( widget.pelicula.personajes ,textAlign: TextAlign.center , style: TextStyle(fontSize: 16.0))),

              )
            )
            
          ],),
          Row(children:<Widget> [
            Expanded(
              child:SizedBox(
                
                child:padding(TextButton(style: TextButton.styleFrom(primary: Colors.white,backgroundColor: Colors.blue,onSurface: Colors.grey,),onPressed: () {Navigator.pop(context); }, child: const Text('Done'),)),

              )
            )
            
          ],),
        ],
      )
    ),
    );
    
    }
    Widget padding(Widget widget){
    return Padding(padding:EdgeInsets.all(7.0),child:widget);
  }
}