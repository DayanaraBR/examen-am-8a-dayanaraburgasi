
import 'package:flutter/material.dart';

import '../model/backend.dart';
import '../widgets/PeliculaWidget.dart';

class ListadoPeliculas extends StatelessWidget{
  final Backend _backend;

  const ListadoPeliculas({Key? key, required Backend backend}) : _backend = backend, super(key: key);

  Widget build(BuildContext context){

    return Scaffold(
      body: ListView(
        
        children: _backend.getPeliculas().map((pelicula) => PeliculaWidget(pelicula)).toList(),
        
      ),
    );
  }
}