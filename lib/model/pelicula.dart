import 'package:flutter/foundation.dart';

class Pelicula {
  int id;
  String foto;
  String name;
  String type;
  String duracion;
  String description;
  String personajes;
  

  Pelicula({
    required this.id,
    required this.foto,
    required this.name,
    required this.type,
    required this.duracion,
    required this.description,
    required this.personajes
    
  });
}

