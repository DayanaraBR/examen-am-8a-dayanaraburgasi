import 'package:dayanara_burgasi/model/pelicula.dart';

class Backend {
 
  static final Backend _backend = Backend._internal();

  var peliculas;

  factory Backend() {
    return _backend;
  }

  Backend._internal();

// coleccion de datos 

final _peliculas= [
  Pelicula(id:1 ,foto: "img/tierra_osos.jpg",name: 'Tierra de Osos', type: 'Infantil/Aventura', duracion: '1h 25m', description: 'Un joven que busca vengar la muerte de su hermano se transforma en oso y hace amistad con un cachorro que acaba de perder a su madre', personajes: 'Kenai,Koda,Denahi,Sitka,Rutt,Tuke'),
  Pelicula(id:2 ,foto: 'img/cusco.jpg',name: 'Las locuras del emperador', type: ' Infantil/Comedia', duracion: '1h 18m', description: 'Yzma, la consejera del arrogante emperador Kuzco, lo transforma en llama. Para regresar a su forma humana, deberá aceptar la ayuda de Pacha.', personajes: 'El Emperador Kuzco,Pacha,Kronk,Yzma,Malina'),
  Pelicula(id:3 ,foto: 'img/rey.jpg',name: 'El rey Leon', type: ' Musical/Infantil', duracion: '1h 27m', description: 'Tras la muerte de su padre, Simba vuelve a enfrentar a su malvado tío, Scar, y reclamar el trono de rey.', personajes: 'Simba,Mufasa,Scar,NAla,Timon,Pumba,Zazu,Rafiki'),
  Pelicula(id:4 ,foto: 'img/rapunzel.jpg',name: 'Enredados', type: 'Infantil/Aventura', duracion: '1h 40m', description: 'Rapunzel, quien fue confinada a una torre cuando era pequeña, desea salir al mundo exterior. Cuando el ladrón más buscado del reino, Flynn Rider, se esconde en la torre, la adolescente decide hacer un trato con él', personajes: 'Rapunzel,Flyn Rider,Madre,Hermano Stabbington'),
  Pelicula(id:5 ,foto: 'img/lilo.jpg',name: 'Lilo y Stitch', type: 'Infantil/Aventura', duracion: '1h 25m', description: 'Una niña solitaria adopta a un perro que en realidad es un extraterrestre travieso que se esconde de unos cazadores intergalácticos.', personajes: 'Stitch,Nani,Lilo,Jumba,Teniente Pleakley,Gantu'),
  Pelicula(id:6 ,foto: 'img/tigger.jpg',name: 'La película de Tigger', type: 'Musical/Infantil', duracion: '1h 17m', description: 'innie Pooh, Piglet, Conejo, Búho, Cangu y Rito están construyéndole una casa a Igor, el burro tristón. Tigger molesta e incordia el grupo y Conejo, harto, le dice que se vaya con su familia, pero Tigger responde que es el único de su especie que existe. ', personajes: 'Tigger,Rito,Winnie Pooh,Igor,Christopher'),
  Pelicula(id:7 ,foto: 'img/monster.jpg',name: 'Monsters, Inc.', type: 'Comedia/Aventura', duracion: '1h 36m', description: 'Monsters, Incorporated es la fábrica de sustos más grande en el mundo de los monstruos y James P. Sullivan es uno de sus mejores asustadores. Sullivan es un monstruo grande e intimidante de piel azul con grandes manchas color púrpura y cuernos', personajes: 'Sullivan,Mike Wazowki,Boo,Roz,Randall,Celia'),
  Pelicula(id:8 ,foto: 'img/toy.jpg',name: 'Toy Story', type: 'Infantil/Comedia', duracion: '1h 21m', description: 'Woody, el juguete favorito de Andy, se siente amenazado por la inesperada llegada de Buzz Lightyear, el guardián del espacio.', personajes: 'Woddy,Buzz,Andy,Slinky,Rex,Señor cara de papa'),
  Pelicula(id:9 ,foto: 'img/atlantis.jpg',name: 'Atlantis: el imperio perdido', type: 'Aventura/Infantil', duracion: '1h 35m', description: 'El nieto de un aventurero se une a la actual expedición para encontrar la legendaria ciudad sumergida.', personajes: 'Kida,Milo,Audrey,Helga,Gaetan,Santorini'),
  Pelicula(id:10 ,foto: 'img/zorro.jpg',name: 'El zorro y el sabueso', type: ' Infantil/Musical ', duracion: '1h 23m', description: 'Un zorrito y un perrito se hacen amigos, pero las circunstancias hacen que se enfrenten cuando son mayores.', personajes: 'Tovy,Tod,Vixey,Amo Slade,Jefe,Big Mama')
  
];


  //Devolver la listas de peliculas
  List<Pelicula> getPeliculas() {
    return _peliculas;
  }

  
  void markPeliculassRead(int id) {
    final index = _peliculas.indexWhere((pelicula) => pelicula.id == id);
    
    peliculaWidget(pelicula) {}

  }
}