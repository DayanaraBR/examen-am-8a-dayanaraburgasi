import 'package:dayanara_burgasi/widgets/appBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}): super(key: key);

  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Prueba de Aplicaciones Moviles',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home:appBarPrincipal() ,
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);
  
  

  

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    return const Scaffold(
     
    );
  }
}
