import 'package:dayanara_burgasi/screens/ListadoPeliculas.dart';
import 'package:flutter/material.dart';
import '../model/backend.dart';

class appBarPrincipal extends StatelessWidget {
  const appBarPrincipal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = 'Dayanara Burgasi';
    return Scaffold(
      appBar: AppBar(
        title: Text('$name     8voA'),
        centerTitle: true,
      ),
      body: ListadoPeliculas(backend: Backend()),
    );
  }
}