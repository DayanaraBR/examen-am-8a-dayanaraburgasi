import 'package:dayanara_burgasi/model/pelicula.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../screens/DetallePeliculas.dart';

class PeliculaWidget extends StatelessWidget{
  final Pelicula pelicula;
  const PeliculaWidget(this.pelicula) ;
  
  @override
  Widget build(BuildContext context) {
    return Card(
      child:GestureDetector(
        onTap: () {
          Navigator.push(context,MaterialPageRoute(builder: (context) => DetallePeliculas(pelicula:pelicula, context:context),),);
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          
      child:Column(
        children:<Widget> [
          Container(
            height: 140.0,
            width: 500.0,
            child: Image.asset(pelicula.foto,height: 140.0,width:160.0),

          ),
          padding(Text(pelicula.name ,style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold))),
          Row(children:<Widget> [
            padding(Text(pelicula.duracion,style: TextStyle(fontSize: 14.0,color: Colors.black45))),
            padding(Text(pelicula.type ,style: TextStyle(fontSize: 16.0))),
          ],),
          /*GestureDetector(
              onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetallePeliculas(pelicula:pelicula, context:context),
                        ),
                      );
              },        
          ),   */ 
        ],
      )
    ),
      ),
      
      
    );
  }

  Widget padding(Widget widget){
    return Padding(padding:EdgeInsets.all(7.0),child:widget);
  }
}